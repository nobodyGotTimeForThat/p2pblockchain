package org.p2p.logger;

public class Logger {

	public static void log(String TAG, String message) {
		System.out.println(TAG + ": " + message);
	}
}
