package org.p2p.comunication.model;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Objects;

public class Peer {
	public static final String delimiter = ":";

	private InetAddress address;
	private int port;

	public Peer(InetAddress address, int port) {
		this.address = address;
		this.port = port;
	}

	public Peer(String string) throws UnknownHostException {
		String[] strings = string.split(delimiter);
		address = InetAddress.getByName(strings[0]);
		port = Integer.getInteger(strings[1]);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Peer peer = (Peer) o;
		return port == peer.port && address.equals(peer.address);
	}

	@Override
	public int hashCode() {
		return Objects.hash(address, port);
	}

	public InetAddress getAddress() {
		return address;
	}

	public void setAddress(InetAddress address) {
		this.address = address;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String toString() {
		return address.toString() + delimiter + port;
	}
}
