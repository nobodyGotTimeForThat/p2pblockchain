package org.p2p.comunication.model;

import org.p2p.comunication.interfaces.Streamable;

import java.net.UnknownHostException;
import java.util.Date;
import java.util.Objects;

public class Message implements Streamable {
	public static final String ping = "PING";
	public static final String pingReturn = "PING_RETURN";

	private static final String delimiter = "<:>";

	private final Peer addressee;
	private Peer lastHop;
	private final Peer sender;
	private final Date timeSent = new Date();
	private final String type;

	public Message(Peer sender, Peer addressee, String type) {
		this.addressee = addressee;
		this.sender = sender;
		this.type = type;
	}

	public Message(String string) throws UnknownHostException {
		String[] strings = string.split(delimiter);
		addressee = new Peer(strings[0]);
		lastHop = new Peer(strings[1]);
		sender = new Peer(strings[2]);
		timeSent.setTime(Long.getLong(strings[3]));
		type = strings[4];
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Message message = (Message) o;
		return Objects.equals(addressee, message.addressee) && Objects.equals(sender, message.sender) && Objects.equals(timeSent, message.timeSent) && Objects.equals(type, message.type);
	}

	@Override
	public int hashCode() {
		return Objects.hash(addressee, sender, timeSent, type);
	}

	@Override
	public String toString() {
		String lastHopString = lastHop != null ? lastHop.toString() : "null";
		return addressee.toString() + delimiter + lastHopString + delimiter + sender.toString() + delimiter + timeSent.getTime() + delimiter + type;
	}

	public String getType() {
		return type;
	}

	public Peer getSender() {
		return sender;
	}

	public Peer getAddressee() {
		return addressee;
	}

	public Peer getLastHop() {
		return lastHop;
	}

	public void setLastHop(Peer lastHop) {
		this.lastHop = lastHop;
	}
}
