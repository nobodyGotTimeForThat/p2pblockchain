package org.p2p.comunication.interfaces;

import org.p2p.comunication.model.Message;

import java.net.Socket;

@FunctionalInterface
public interface MessageProcessor {
	void process(Message message);
}
