package org.p2p.comunication.interfaces;

import java.nio.charset.StandardCharsets;

public interface Streamable {
	static byte[] toBytes(Object data){
		return data.toString().getBytes(StandardCharsets.UTF_8);
	}

	static String getStringFromBytes(byte[] bytes) {
		return new String(bytes, StandardCharsets.UTF_8);
	}
}
