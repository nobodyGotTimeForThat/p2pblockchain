package org.p2p.comunication;

import org.p2p.comunication.interfaces.Streamable;
import org.p2p.comunication.model.Message;
import org.p2p.comunication.model.Peer;
import org.p2p.logger.Logger;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class Transmitter implements Runnable{
	public static final String TAG = "Transmitter";

	private boolean isActive = false;
	private Receiver myReceiver;
	private final List<Message> messageQueue;
	private final List<Peer> peers;

	public Transmitter() {
		messageQueue = new ArrayList<>();
		peers = new ArrayList<>();
	}

	public void addPeer(Peer peerToBeAdded) {
		if (peers.stream().noneMatch(peer -> peerToBeAdded.equals(peer))) {
			peers.add(peerToBeAdded);
		}
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean active) {
		isActive = active;
	}

	@Override
	public void run() {
		Logger.log(TAG, "Starting transmitter");
		isActive = true;

		Logger.log(TAG, "Transmitter started");
		while (isActive)
		{
			if (!messageQueue.isEmpty()) {
				messageQueue.stream().parallel().forEach(message -> {
					Peer targetPeer = getPeer(message.getAddressee());
					if (targetPeer != null) {
						// send message to peer directly when the peer is in registry
						sendMessageToPeer(message, targetPeer);
					}
					else {
						// broadcast message to all peers and hope it gets to addressee
						peers.stream().parallel().forEach(peer -> {
							sendMessageToPeer(message, peer);
						});
					}
				});
			}
		}
	}

	public void sendMessage(Message message){
		messageQueue.add(message);
		Logger.log(TAG, "Message: '" + message.toString() + "' add to sending queue");
	}

	private Peer getPeer(Peer target){
		Optional<Peer> peer = peers.stream().filter(peer1 -> peer1.equals(target)).findFirst();

		return peer.orElse(null);

	}

	private void sendMessageToPeer(Message message, Peer peer) {
		try {
			message.setLastHop(myReceiver.getAddress());
			Logger.log(TAG, "Sending message: '" + message.toString() + "'");
			Socket socket = new Socket(peer.getAddress(), peer.getPort());
			socket.getOutputStream().write(Streamable.toBytes(message.toString()));
			socket.getOutputStream().flush();
			socket.getOutputStream().close();
			socket.close();
			Logger.log(TAG, "Message: '" + message.toString() + "' sent");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setMyReceiver(Receiver myReceiver) {
		this.myReceiver = myReceiver;
	}
}
