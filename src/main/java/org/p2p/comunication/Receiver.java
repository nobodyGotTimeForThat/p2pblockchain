package org.p2p.comunication;

import org.p2p.comunication.interfaces.MessageProcessor;
import org.p2p.comunication.interfaces.Streamable;
import org.p2p.comunication.model.Message;
import org.p2p.comunication.model.Peer;
import org.p2p.logger.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Receiver implements Runnable{
	public static final String TAG = "Receiver";

	private boolean isActive = false;
	private final int port;
	private MessageProcessor messageProcessor = null;
	private ServerSocket serverSocket;

	public Receiver(int port) {
		this.port = port;
	}

	public Peer getAddress(){
		if (isActive)
		{
			return new Peer(serverSocket.getInetAddress(), serverSocket.getLocalPort());
		}

		return null;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean active) {
		isActive = active;
	}

	@Override
	public void run(){
		Logger.log(TAG, "Starting receiver at port: " + port);

		try {
			serverSocket = new ServerSocket(port);
			isActive = true;
		} catch (IOException e) {
			e.printStackTrace();
		}

		Logger.log(TAG, "Receiver started at '" + getAddress().toString() + "'");
		while(isActive)
		{
			Socket clientSocket = null;
			try {
				clientSocket = serverSocket.accept();
				if (clientSocket != null && messageProcessor != null) {
					Message message = new Message(Streamable.getStringFromBytes(clientSocket.getInputStream().readAllBytes()));
					messageProcessor.process(message);
				}

				serverSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void setMessageProcessor(MessageProcessor messageProcessor) {
		this.messageProcessor = messageProcessor;
	}
}
