package org.p2p;

import org.p2p.comunication.Receiver;
import org.p2p.comunication.Transmitter;
import org.p2p.comunication.interfaces.MessageProcessor;
import org.p2p.comunication.model.Message;
import org.p2p.comunication.model.Peer;
import org.p2p.logger.Logger;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Main {
	public static final String TAG = "Comrade";
	public static final int receiverPort = 6969;
	public static final int sleepMillis = 3000;

	public static void main(String[] args) {
		List<Message> messagesHistory = new ArrayList<>();

		// Initialize Transmitter
		Transmitter transmitter = new Transmitter();

		// Initialize Receiver
		Receiver receiver = new Receiver(receiverPort);
		receiver.setMessageProcessor(message -> {
			try {
				transmitter.addPeer(message.getLastHop());
				addMessageToHistory(messagesHistory, message);

				if (!message.getAddressee().equals(receiver.getAddress())) {
					if (messagesHistory.stream().noneMatch(message1 -> message.equals(message1))) {
						Logger.log(Receiver.TAG, "Got message: '" + message.toString() + "'. Message is not for me, adding to sending queue");
						transmitter.sendMessage(message);
					}
					return;
				}

				switch (message.getType()) {
					case Message.ping -> {
						transmitter.sendMessage(new Message(receiver.getAddress(), message.getSender(), Message.pingReturn));
					}
					case Message.pingReturn -> {

					}
					default -> {
					}
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		});

		Logger.log(TAG, "Starting receiver");
		// Start Receiver on separate thread
		Thread receiverThread = new Thread(receiver);
		receiverThread.start();

		Logger.log(TAG, "Starting transmitter");
		// Start Transmitter on separate thread
		transmitter.setMyReceiver(receiver);
		Thread transmitterThread = new Thread(transmitter);
		transmitterThread.start();

		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// Simulation block
		try {
			// add peers
			transmitter.addPeer(new Peer(InetAddress.getByName("127.0.0.1"), 6969));

			// add targets
			List<Peer> targets = new ArrayList<>();
			targets.add(new Peer(InetAddress.getByName("127.0.0.1"), 6969));

			// simulate
			for (Peer target : targets ) {
				for (int i = 0; i < 30; i++) {
					transmitter.sendMessage(new Message(receiver.getAddress(), target, Message.ping));
					Thread.sleep(sleepMillis);
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		Logger.log(TAG, "Shutting down receiver");
		// Shutdown Receiver
		receiver.setActive(false);

		Logger.log(TAG, "Shutting down transmitter");
		// Shutdown Transmitter
		transmitter.setActive(false);
	}

	public static void addMessageToHistory(List<Message> history, Message message) {
		if (history.stream().noneMatch(message1 -> message.equals(message1))) {
			history.add(message);
		}
	}
}
